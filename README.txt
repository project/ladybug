Please read this file in its entirety before installing this theme.

#WARNINGS#

1. USE A SUB-THEME, IT'S FOR YOUR OWN GOOD

    If you plan on making changes, I strongly recommend that you
    create a sub-theme.  Creating a sub-theme is by far the best way
    to ensure your changes will not be overridden. It is also invaluable
    when it comes to debugging because you can easily see determine if
    there are bugs with the Ladybug theme or with your code.  If you don't
    know how to create one, see this page: http://drupal.org/node/225125

2. ENABLING TRANSPARENCY SUPPORT FOR IE6

    The Ladybug theme comes with transparency support for Internet Explorer
    6 using jquery.pngFix.js (although I did not actually test whether it
    works!) It should work as is, if not and you find a fix, please, share!

#NOTES#

Most of the files were created by the author of the Sky theme.

    http://drupal.org/project/sky

Some comments may not match the new theme exactly.

#INSTALLATION#

1. Choose a directory for your themes.

It is strongly recommended that you do NOT install this theme in the Drupal
theme directory (the directory that contains Garland), because this is
considered "hacking core" and because when you upgrade Drupal itself, you
risk overwriting any changes you made, or on a Mac, loosing the theme
entirely.  It is recommended that you install the theme in one of the
following directories:

  * /sites/all/themes/ladybug
  * /sites/default/themes/ladybug
  * /sites/mysite/themes/ladybug (if you are running a multi-site setup
                                  and do not want to share)

2. Visit the admin/build/themes page

On that page you can enable and set the Ladybug theme as the default theme.

3. Configure the theme

Once the theme enabled, visit the theme settings page, by clicking the
"configure" tab on the admin/build/themes page.  There you will
find quite a few settings that you can customize. (all were not tested
with Ladybug, those come from Sky.)

#TROUBLESHOOTING#

* Verify the permissions of your .../files/ directory

Make sure you have the files directory has been created and has the
correct permissions assigned. Drupal should take care of this during the
installation process, but if you are getting errors that Drupal cannot
create the file named ladybug.css, it is likely due to permissions
issues or the lack of a files directory.

This is required to make changes to the configuration. The theme works
without it.
